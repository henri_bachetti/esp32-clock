# ESP32 MATRIX CLOCK

The purpose of this page is to explain step by step the realization of an ESP32 matrix clock.

The brightness and display mode are configured using 2 touch sensors.

The display modes are :
 * day of the week + day of the month
 * day of the month + month
 * day of the week + day of the month + month + year + saint of the day (scrolling mode)

The board uses the following components :

 * an ESP32 board (DEV-KIT for example)
 * 8 MAX7219 matrix modules (or 2 x quad modules)
 * 2 touch sensors
 * a 220µF capacitor (3300µF if high brightness is used)
 * the board is powered by the USB connector (or 5V external power if high brightness is used).

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/05/max7219-affichage-matriciel.html

