
#include <Arduino.h>
#include <EEPROM.h>

#define LOG_DOMAIN LOG_CONFIG
#include "debug.h"
#include "ephemeris.h"
#include "config.h"

String ntpUrl = "pool.ntp.org";

int saveConfiguration(void)
{
  struct eepromConfig eeprom;

  log_print("ephemeris: "); log_println(ephemerisUrl);
  log_print("ntp: "); log_println(ntpUrl);
  log_print("brightness: "); log_println(brightness);
  log_print("display: "); log_println(displayMode);
  strncpy(eeprom.ephemerisUrl, ephemerisUrl.c_str(), sizeof(eeprom.ephemerisUrl));
  strncpy(eeprom.ntpUrl, ntpUrl.c_str(), sizeof(eeprom.ntpUrl));
  eeprom.brightness = brightness;
  eeprom.magic = MAGIC;
  EEPROM.put(0, eeprom);
  if (EEPROM.commit()) {
    log_println("EEPROM successfully committed");
    return true;
  } else {
    log_println("ERROR! EEPROM commit failed");
    return false;
  }
}

int loadConfiguration(void)
{
  struct eepromConfig eeprom;

  EEPROM.get(0, eeprom);
  if (eeprom.magic == MAGIC) {
    log_println("configuration:");
    ephemerisUrl = eeprom.ephemerisUrl;
    ntpUrl = eeprom.ntpUrl;
    brightness = min((int)eeprom.brightness, 15);
    displayMode = min((int)eeprom.displayMode, DISPLAY_SCROLL);
    log_print("ephemeris: "); log_println(ephemerisUrl);
    log_print("ntp: "); log_println(ntpUrl);
    log_print("brightness: "); log_println(brightness);
    log_print("display: "); log_println(displayMode);
  }
  else {
    log_println("no Configuration found");
  }
}
