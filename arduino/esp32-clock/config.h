
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <NTP.h>

#define DEFAULT_EPHEMERIS_URL "http://192.168.1.134/ephemeris.php"

#define MAGIC                   0xDEADBEEF
#define DISPLAY_WDM             0
#define DISPLAY_DM              1
#define DISPLAY_SCROLL          2

struct eepromConfig
{
  char ephemerisUrl[50];
  char ntpUrl[50];
  byte brightness;
  byte displayMode;
  uint32_t magic;
};

extern String ntpUrl;
extern byte brightness;
extern byte displayMode;

int saveConfiguration(void);
int loadConfiguration(void);

#endif
