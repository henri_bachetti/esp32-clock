
#ifndef _DEBUG_H_
#define _DEBUG_H_

#define LOG_MAIN        0x01
#define LOG_CONFIG      0x02
#define LOG_DOMOTICZ    0x04
#define LOG_EPHEMERIS   0x08
#define LOG_INFOS       0x10
#define LOG_TEMP        0x20
#define LOG_TFT         0x40
#define LOG_WEATHER     0x80

#define LOGS_ACTIVE     (LOG_MAIN | LOG_CONFIG | LOG_EPHEMERIS | LOG_INFOS | LOG_TEMP | LOG_WEATHER)

#if (LOGS_ACTIVE & LOG_DOMAIN)
#define log_print       Serial.print
#define log_println     Serial.println
#define log_printf      Serial.printf
#else
#define log_print
#define log_println
#define log_printf
#endif

#endif
