
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_EPHEMERIS
#include "utf8.h"
#include "ephemeris.h"

#define DEFAULT_EPHEMERIS_URL "http://192.168.1.134/ephemeris.php"

String ephemerisUrl(DEFAULT_EPHEMERIS_URL);

void getSaintOfTheDay(char *saint, int len)
{
  HTTPClient http;
  // EPHEMERIS
  http.begin(ephemerisUrl);
  int httpCode = http.GET();
  if (httpCode > 0) {
    String html = http.getString();
    String name = html.substring(html.indexOf("<label>") + 7, html.indexOf("</label>"));
    strncpy(saint, name.c_str(), len);
  }
  http.end();
}
