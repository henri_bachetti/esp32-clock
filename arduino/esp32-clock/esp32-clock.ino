//
// A simple server implementation showing how to:
//  * serve static messages
//  * read GET and POST parameters
//  * handle missing pages / 404s
//

#include <Arduino.h>
#include <EEPROM.h>
#ifdef ESP32
#include <WiFi.h>
#include <ESPmDNS.h>
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>
#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

#define LOG_DOMAIN    LOG_MAIN
#include "debug.h"
#include "config.h"
#include "fonts-data.h"
#include "utf8.h"
#include "ephemeris.h"

AsyncWebServer server(80);

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CLK_PIN   18 // or SCK
#define DATA_PIN  23 // or MOSI
#define PAUSE_TIME    0
#define FRAME_TIME    50

MD_Parola hDisplay = MD_Parola(HARDWARE_TYPE, 26, MAX_DEVICES);
MD_Parola dDisplay = MD_Parola(HARDWARE_TYPE, 25, MAX_DEVICES);
#define NUM_BUTTONS         4
#define BTN_BRIGHTNESS      0
#define BTN_MODE            1
#define TOUCH_THRESHOLD     20
const uint8_t buttonPin[NUM_BUTTONS] = {4, 15};
byte brightness;
byte displayMode;

const char* ssid = "........";
const char* password = "........";
const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

void test(MD_MAX72XX *pDisplay)
{
  for (uint8_t row = 0 ; row < ROW_SIZE ; row++) {
    for (uint16_t col = 0 ; col < COL_SIZE * MAX_DEVICES ; col++) {
      pDisplay->setPoint(row, col, 1);
    }
  }
  pDisplay->control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
  delay(1000);
  pDisplay->transform(MD_MAX72XX::TINV);
}

void setup()
{
  Serial.begin(115200);
  EEPROM.begin(1024);
  loadConfiguration();
  hDisplay.begin();
  dDisplay.begin();
  hDisplay.setIntensity(brightness);
  dDisplay.setIntensity(brightness);
  hDisplay.setFont(ExtASCII);
  dDisplay.setFont(ExtASCII);
  MD_MAX72XX *pDisplay = hDisplay.getGraphicObject();
  test(pDisplay);
  pDisplay = dDisplay.getGraphicObject();
  test(pDisplay);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  log_print("connecting to "); log_println(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    log_print(".");
  }
  log_println("");
  log_print("Connected to "); log_println(ssid);
  log_print("IP address: "); log_println(WiFi.localIP());
  if (MDNS.begin("esp32")) {
    log_println("MDNS responder started");
  }
  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpUrl.c_str());
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(200, "text/plain", "Hello, world");
  });
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String message;
    if (request->hasParam(PARAM_MESSAGE)) {
      message = request->getParam(PARAM_MESSAGE)->value();
    } else {
      message = "No message sent";
    }
    request->send(200, "text/plain", "Hello, GET: " + message);
  });
  server.on("/post", HTTP_POST, [](AsyncWebServerRequest * request) {
    String message;
    if (request->hasParam(PARAM_MESSAGE, true)) {
      message = request->getParam(PARAM_MESSAGE, true)->value();
    } else {
      message = "No message sent";
    }
    request->send(200, "text/plain", "Hello, POST: " + message);
  });
  server.onNotFound(notFound);
  server.begin();
}

void loop()
{
  const char *weekDay[] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};
  const char *shortWeekDay[] = {"Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"};
  const char *month[] = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
  static unsigned long lastDisplayTime;
  static unsigned long touchDelay;
  static unsigned long funcDisplayTime;
  static byte day;
  static char timeString[10];
  static char dateString[70];
  static char saint[40];
  static char funcString[20];
  struct tm timeinfo;

  if (touchDelay == 0) {
    touchRead(buttonPin[BTN_BRIGHTNESS]);
    if (touchRead(buttonPin[BTN_BRIGHTNESS]) < TOUCH_THRESHOLD) {
      touchDelay = funcDisplayTime = millis();
      brightness = brightness < 15 ? brightness + 1 : 0;
      hDisplay.setIntensity(brightness);
      dDisplay.setIntensity(brightness);
      snprintf(funcString, sizeof(funcString), "lum: %d", brightness + 1);
      log_printf("%s\n", funcString);
      dDisplay.displayText(funcString, PA_LEFT, FRAME_TIME, PAUSE_TIME, PA_PRINT, PA_NO_EFFECT);
      dDisplay.displayReset();
      saveConfiguration();
    }
    touchRead(buttonPin[BTN_MODE]);
    if (touchRead(buttonPin[BTN_MODE]) < TOUCH_THRESHOLD) {
      touchDelay = funcDisplayTime = millis();
      const char *mode[] = {"WDM", "DM", "Scroll"};
      displayMode = displayMode < DISPLAY_SCROLL ? displayMode + 1 : DISPLAY_WDM;
      log_printf("diplay mode %d\n", displayMode);
      snprintf(funcString, sizeof(funcString), "%s", mode[displayMode]);
      log_printf("%s\n", funcString);
      dDisplay.displayText(funcString, PA_LEFT, FRAME_TIME, PAUSE_TIME, PA_PRINT, PA_NO_EFFECT);
      dDisplay.displayReset();
      saveConfiguration();
    }
  }
  if (millis() - touchDelay >= 500) {
    touchDelay = 0;
  }
  if (!getLocalTime(&timeinfo)) {
    log_println("Failed to obtain time");
    return;
  }
  if (dDisplay.displayAnimate()) {
    if (day != timeinfo.tm_mday) {
      day = timeinfo.tm_mday;
      getSaintOfTheDay(saint, sizeof(saint));
    }
    if (funcDisplayTime) {
      if (millis() - funcDisplayTime >= 2000) {
        funcDisplayTime = 0;
      }
      dDisplay.displayText(funcString, PA_LEFT, FRAME_TIME, PAUSE_TIME, PA_PRINT, PA_NO_EFFECT);
    }
    else {
      switch (displayMode) {
        case DISPLAY_WDM: // display week day and day number
          snprintf(dateString, sizeof(dateString), "%s %d", shortWeekDay[timeinfo.tm_wday], timeinfo.tm_mday);
          utf8Ascii(dateString);
          dDisplay.displayText(dateString, PA_CENTER, FRAME_TIME, PAUSE_TIME, PA_PRINT, PA_NO_EFFECT);
          break;
        case DISPLAY_DM: // display day and month number
          snprintf(dateString, sizeof(dateString), "%02d/%02d", timeinfo.tm_mday, timeinfo.tm_mon);
          utf8Ascii(dateString);
          dDisplay.displayText(dateString, PA_CENTER, FRAME_TIME, PAUSE_TIME, PA_PRINT, PA_NO_EFFECT);
          break;
        case DISPLAY_SCROLL: // display week day, day, month (string) and year + saint of the day (scrolling)
          snprintf(dateString, sizeof(dateString), "%s %d %s %d - %s", weekDay[timeinfo.tm_wday], timeinfo.tm_mday, month[timeinfo.tm_mon], timeinfo.tm_year + 1900, saint);
          utf8Ascii(dateString);
          dDisplay.displayText(dateString, PA_LEFT, FRAME_TIME, PAUSE_TIME, PA_SCROLL_LEFT, PA_SCROLL_LEFT);
          break;
      }
      log_println(dateString);
    }
  }
  if (hDisplay.displayAnimate()) {
    if (millis() - lastDisplayTime >= 1000) {
      lastDisplayTime = millis();
      if (timeinfo.tm_sec % 2) {
        strftime(timeString, sizeof(timeString), "%H:%M", &timeinfo);
      }
      else {
        strftime(timeString, sizeof(timeString), "%H\x1f%M", &timeinfo);
      }
      hDisplay.displayText(timeString, PA_CENTER, 0, 0, PA_PRINT, PA_NO_EFFECT);
    }
  }
}
